import React from 'react';
import ReactDOM from 'react-dom';
import {
    Collapse,
    Dropdown,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavbarToggler,
    NavItem,
    NavLink,
    UncontrolledDropdown
} from 'reactstrap';
import Cookies from 'js-cookie';
import Login from './Login';
import constants from '../constants';
import axios from 'axios';
import {withTranslation} from 'react-i18next';
import i18n from "i18next";

class MyNavbar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpen: true
        };
    }

    logOut = () => {
        axios.get(constants.url + '/auth/signout', {headers: {"Authorization": "Bearer " + constants.token}}).then(
            res => {
                constants.isLoggedIn = "false";
                constants.token = null;
                Cookies.remove('isLoggedIn');
                Cookies.remove('token');
                ReactDOM.render(<Login/>, document.getElementById('root'));
            }
        )
    };

    toggle = () => {
        this.setState(
            {
                isOpen: !this.state.isOpen
            }
        )
    };

    changeLanguage = (key) => {
        i18n.changeLanguage(key)
    };

    render() {
        const t = this.props.t;
        return (
            <Navbar sticky="top" color="dark" dark expand="md">
                <NavbarBrand href="/">{t('lbl_header')}</NavbarBrand>
                <NavbarToggler onClick={this.toggle}/>
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                href="https://www.gittigidiyor.com/BanaOzel/sattiklarim.php">Gittigidiyor</NavLink>
                        </NavItem>
                        <UncontrolledDropdown nav inNavbar>
                            <DropdownToggle nav caret>
                                {t('lbl_settings')}
                            </DropdownToggle>
                            <DropdownMenu right>
                                <Dropdown direction="left" isOpen={this.state.btnDropleft} toggle={() => {
                                    this.setState({btnDropleft: !this.state.btnDropleft});
                                }}>
                                    <DropdownToggle caret color="primary" className="dropdown-item">
                                        {t('lbl_language')}
                                    </DropdownToggle>
                                    <DropdownMenu>
                                        {i18n.options.preload.map((lang) =>
                                            (
                                                <DropdownItem active={i18n.language === lang.key} key={lang.key}
                                                              onClick={() => this.changeLanguage(lang.key)}>{lang.name}</DropdownItem>
                                            )
                                        )}
                                    </DropdownMenu>
                                </Dropdown>
                                <DropdownItem color="danger" onClick={() => this.logOut()}>
                                    {t('lbl_exit')}
                                </DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </Nav>
                </Collapse>
            </Navbar>
        );
    }
}

export default withTranslation()(MyNavbar);