import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import './Login.css';
import axios from 'axios';
import constants from '../constants'
import App from '../App';
import Cookies from 'js-cookie';
import {Spin, message} from 'antd';
import {withTranslation} from "react-i18next";

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            loading: false
        };
    }

    login = () => {
        this.setState({loading: true});
        axios.post(constants.url + '/auth/signin', {
                username: this.state.username,
                password: this.state.password
            }
        )
            .then(res => {
                constants.token = res.data.token;
                constants.isLoggedIn = "true";
                Cookies.set('isLoggedIn', "true", {expires: 1 / 60});
                Cookies.set('token', res.data.token, {expires: 1 / 60});
                ReactDOM.render(<App/>, document.getElementById('root'));
            })
            .catch(error => {
                message.error(this.props.t('msg_login_fail'));
                console.log(error);
            }).finally(() => {
            this.setState({loading: false});
        });
    }

    loginAsTest = () => {
        axios.post(constants.url + '/auth/signin', {
                username: 'user',
                password: 'user'
            }
        )
            .then(res => {
                constants.token = res.data.token;
                constants.isLoggedIn = "true";
                Cookies.set('isLoggedIn', "true", {expires: 1 / 60});
                Cookies.set('token', res.data.token, {expires: 1 / 60});
                ReactDOM.render(<App/>, document.getElementById('root'));
            })
            .catch(error => {
                message.error(this.props.t('msg_login_fail'));
                console.log(error);
            });
    };

    register = () => {
        this.setState({loading: true});
        axios.post(constants.url + '/api/user', {
                username: this.state.username,
                password: this.state.password
            }
        )
            .then(res => {
                message.success(this.props.t('msg_register_success'));
            })
            .catch(error => {
                message.error(this.props.t('msg_register_fail'));
                console.log(error);
            }).finally(() => {
            this.setState({loading: false});
        });
    }

    handleChange = (event) => {
        this.setState({[event.target.name]: event.target.value});
    };

    render() {
        const t = this.props.t;
        return (
            <Spin spinning={this.state.loading}>
                <div className="container">
                    <div className="row">
                        <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
                            <div className="card card-signin my-5">
                                <div className="card-body text-center">
                                    <img className="img-fluid" width="150dp" alt="icon"
                                         src={require('../assets/cemboylu2.png')}/>
                                    <br/>
                                    <br/>
                                    <form className="form-signin">
                                        <div className="form-label-group">
                                            <input value={this.state.username} onChange={this.handleChange} type="email"
                                                   name="username" id="inputEmail" className="form-control" required
                                                   autoFocus/>
                                            <label htmlFor="inputEmail">{t('lbl_email')}</label>
                                        </div>
                                        <div className="form-label-group">
                                            <input value={this.state.password} onChange={this.handleChange}
                                                   type="password"
                                                   name="password" id="inputPassword" className="form-control"
                                                   required/>
                                            <label htmlFor="inputPassword">{t('lbl_password')}</label>
                                        </div>
                                        <div className="custom-control custom-checkbox mb-3">
                                            <input type="checkbox" className="custom-control-input" id="customCheck1"/>
                                            <label className="custom-control-label"
                                                   htmlFor="customCheck1">{t('lbl_remember')}</label>
                                        </div>
                                        <button className="btn btn-lg btn-primary btn-block text-uppercase"
                                                type="button"
                                                onClick={this.login}>{t('btn_login')}
                                        </button>
                                        <button className="btn btn-lg btn-warning btn-block text-uppercase"
                                                type="button"
                                                onClick={this.loginAsTest}>{t('btn_try_free')}
                                        </button>
                                        <button className="btn btn-lg btn-success btn-block text-uppercase"
                                                type="button"
                                                onClick={this.register}>{t('btn_register')}
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Spin>
        );
    }
}

export default withTranslation()(Login);