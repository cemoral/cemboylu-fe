import {Button, Form, Input, message, Modal, Popconfirm, Table} from 'antd';
import React from 'react';
import "antd/dist/antd.css";
import "./MyTable.css";
import {productService} from '../services/ProductService';
import {withTranslation} from "react-i18next";

class MyTable extends React.Component {
    formRef = React.createRef();

    constructor(props) {
        super(props);
        this.state = {
            filteredInfo: null,
            sortedInfo: null,
            dataSource: [],
            count: 0,
            modalTitle: '',
            modalVisible: false,
            editingRow: null,
            currentPage: 0,
            pageSize: parseInt(window.innerHeight / 53) - 4
        };
        message.config({top: 60});
    }

    componentDidMount = () => {
        productService.getProducts(0, this.state.pageSize).then(res => this.setState({...res}));
    };

    handleDelete = async key => {
        let resp = await productService.deleteProduct(key);
        if (resp) {
            this.setState(prevState => ({dataSource: prevState.dataSource.filter(item => item.code !== key)}));
            message.success(this.props.t('msg_product_delete_success'));
        } else {
            message.error(this.props.t('msg_product_delete_error'));
        }
    };

    handleChange = (pagination, filters, sorter) => {
        productService.getProducts(pagination.current - 1, this.state.pageSize).then(res => this.setState({...res}));
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
            currentPage: pagination.current - 1
        });
    };

    clearFilters = () => {
        this.setState({filteredInfo: null});
    };

    clearAll = () => {
        this.setState({
            filteredInfo: null,
            sortedInfo: null,
        });
    };

    setAgeSort = () => {
        this.setState({
            sortedInfo: {
                order: 'descend',
                columnKey: 'age',
            },
        });
    };

    showModal = (param) => {
        if (param) {
            this.setState({modalVisible: true, modalTitle: this.props.t('lbl_edit_product'), editingRow: param})
            if (this.formRef.current !== null) {
                this.formRef.current.setFieldsValue({...param})
            }
        } else {
            let editingRow = {
                code: '',
                createDate: '',
                description: '',
                name: '',
                purchasePrice: 0.0,
                quantity: 0,
                salePrice: 0.0
            };
            this.setState({modalVisible: true, modalTitle: this.props.t('lbl_add_product'), editingRow: editingRow})
            if (this.formRef.current !== null) {
                this.formRef.current.setFieldsValue({...editingRow})
            }
        }
    };

    handleModalOk = (values) => {
        const {editingRow, modalTitle, dataSource} = this.state;
        if (modalTitle === this.props.t('lbl_add_product')) {
            productService.addProduct(editingRow).then(res => {
                let newProducts = [].concat(editingRow, dataSource);
                this.setState({
                    dataSource: newProducts,
                    modalVisible: false,
                    editingRow: null
                });
                //this.props.form.resetFields();
                message.success(this.props.t('msg_product_add_success'));
            }).catch(err => {
                message.error(this.props.t('msg_product_add_error'));
            });
        } else {
            productService.updateProduct(editingRow).then(res => {
                message.success(this.props.t('msg_product_update_success'));
                //this.props.form.resetFields();
                this.setState({
                    modalVisible: false,
                    editingRow: null
                });
            }).catch(err => {
                message.error(this.props.t('msg_product_update_error'));
            });
        }
    };

    handleModalCancel = (e) => {
        this.setState({modalVisible: false, editingRow: null});

        //this.props.form.resetFields();
    };

    handleVarChange = (e) => {
        const edit = this.state.editingRow;
        edit[e.target.name] = e.target.value;
        this.setState({editingRow: edit});
    };

    search = (e) => {
        let val = e.target.value;
        if (val !== "") {
            this.setState(prevState => ({
                dataSource: prevState.dataSource.filter(p => Object.values(p).toString().toLowerCase().includes(val))
            }));
        } else {
            productService.getProducts(this.state.currentPage, this.state.pageSize).then(res => this.setState({...res}));
        }
    };

    render() {
        let {sortedInfo, filteredInfo, dataSource} = this.state;
        sortedInfo = sortedInfo || {};
        filteredInfo = filteredInfo || {};
        //let {getFieldDecorator} = this.props.form;
        const t = this.props.t;
        const formItemLayout = {
            labelCol: {span: 6},
            wrapperCol: {span: 18}
        };

        const columns = [
            {
                title: t('lbl_product_code'),
                dataIndex: 'code',
                key: 'code',
                //filters: [{ text: 'code', value: 'code' }, { text: 'code', value: 'code' }],
                filteredValue: filteredInfo.code || null,
                onFilter: (value, record) => record.code.includes(value),
                sorter: (a, b) => b.code - a.code,
                sortOrder: sortedInfo.columnKey === 'code' && sortedInfo.order,
                type: 'string'
            },
            {
                title: t('lbl_product_name'),
                dataIndex: 'name',
                key: 'name',
                sorter: (a, b) => a.name.toString().localeCompare(b.name),
                sortOrder: sortedInfo.columnKey === 'name' && sortedInfo.order,
                type: 'string'
            },
            {
                title: t('lbl_product_description'),
                dataIndex: 'description',
                key: 'description',
                //filters: [{ text: 'London', value: 'London' }, { text: 'New York', value: 'New York' }],
                filteredValue: filteredInfo.description || null,
                onFilter: (value, record) => record.description.includes(value),
                sortOrder: false,
                type: 'string'
            },
            {
                title: t('lbl_product_purchase_price') + '\t\u20BA',
                dataIndex: 'purchasePrice',
                key: 'purchasePrice',
                //filters: [{ text: 'London', value: 'London' }, { text: 'New York', value: 'New York' }],
                filteredValue: filteredInfo.purchasePrice || null,
                onFilter: (value, record) => record.purchasePrice.includes(value),
                sorter: (a, b) => b.purchasePrice - a.purchasePrice,
                sortOrder: sortedInfo.columnKey === 'purchasePrice' && sortedInfo.order,
                type: 'number'
            },
            {
                title: t('lbl_product_sale_price') + '\t\u20BA',
                dataIndex: 'salePrice',
                key: 'salePrice',
                //filters: [{ text: 'London', value: 'London' }, { text: 'New York', value: 'New York' }],
                filteredValue: filteredInfo.salePrice || null,
                onFilter: (value, record) => record.salePrice.includes(value),
                sorter: (a, b) => b.salePrice - a.salePrice,
                sortOrder: sortedInfo.columnKey === 'salePrice' && sortedInfo.order,
                type: 'number'
            },
            {
                title: t('lbl_product_quantity'),
                dataIndex: 'quantity',
                key: 'quantity',
                //filters: [{ text: 'London', value: 'London' }, { text: 'New York', value: 'New York' }],
                filteredValue: filteredInfo.quantity || null,
                onFilter: (value, record) => record.quantity.includes(value),
                sorter: (a, b) => b.quantity - a.quantity,
                sortOrder: sortedInfo.columnKey === 'quantity' && sortedInfo.order,
                type: 'number'
            },
            {
                title: t('lbl_action'),
                dataIndex: 'action',
                key: 'action',
                sortOrder: false,
                render: (text, record) =>
                    this.state.dataSource.length >= 1 ? (
                        <div>
                            <a href="/#" onClick={(e) => this.showModal(record)}>{t('lbl_edit')}</a>,
                            &nbsp;
                            <Popconfirm title={t('popup_delete_confirm')}
                                        onConfirm={() => this.handleDelete(record.code)}>
                                <a href="/#">{t('lbl_delete')}</a>
                            </Popconfirm>
                        </div>
                    ) : null,
            },
        ];
        return (
            <div>
                <div style={{display: 'flex'}}>
                    <Input.Search placeholder={t('input_search')} onChange={this.search}/>
                    <Button block type="primary" onClick={() => this.showModal()}>{t('lbl_add_product')}</Button>
                </div>
                <Table columns={columns} dataSource={dataSource} rowKey="code" onChange={this.handleChange}
                       pagination={{pageSize: this.state.pageSize, total: this.state.count}}/>
                <Modal
                    title={this.state.modalTitle}
                    visible={this.state.modalVisible}
                    onOk={this.handleModalOk}
                    onCancel={this.handleModalCancel}
                    okText={t('lbl_ok')}
                    cancelText={t('lbl_cancel')}
                >
                    <Form ref={this.formRef}
                          className="login-form" {...formItemLayout} /*onFinish={this.handleModalOk}*/
                          initialValues={this.state.editingRow}>
                        {columns.filter(c => (c.key !== 'code' || this.state.modalTitle === t('lbl_add_product')) && c.key !== "action").map(c => {
                            return (
                                <Form.Item key={c.key} label={c.title} name={c.key} rules={[{
                                    required: ['code', 'name'].includes(c.key),
                                    type: c.type,
                                    message: t('input_error')
                                }]}
                                           getValueFromEvent={e => {
                                               if (c.type === 'number' && !isNaN(Number(e.currentTarget.value))) {
                                                   return Number(e.currentTarget.value);
                                               } else {
                                                   return e.currentTarget.value;
                                               }
                                           }}>
                                    <Input name={c.key} onChange={this.handleVarChange}/>
                                </Form.Item>)
                        })}
                    </Form>
                </Modal>
            </div>
        );
    }
}

export default withTranslation()(MyTable);
