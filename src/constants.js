import Cookies from 'js-cookie';

export const constants = {
    //url: 'http://localhost:3030',
    url: 'http://192.168.1.100:3030',
    //url: 'http://kopus.ddns.net:3030',
    token: Cookies.get('token') === null || undefined ? undefined : Cookies.get('token'),
    isLoggedIn: Cookies.get('isLoggedIn') === null || undefined ? "false" : Cookies.get('isLoggedIn')
    //url: 'http://54.93.237.211:3030'
};

export default constants;