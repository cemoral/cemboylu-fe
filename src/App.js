import React, {Component} from 'react';
import './App.css';
import MyNavbar from './components/MyNavbar';
import MyTable from './components/MyTable';

class App extends Component {

    render() {
        return (
            <div>
                <MyNavbar/>
                <MyTable/>
            </div>
        );
    }
}

export default App;