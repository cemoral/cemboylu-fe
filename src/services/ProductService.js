import axios from 'axios';
import constants from '../constants';

class ProductService {

    async getProducts(pageNumber, pageSize) {
        let res = await axios.get(constants.url + '/api/product/' + pageNumber + '/' + pageSize, {headers: {"Authorization": "Bearer " + constants.token}});
        return {
            dataSource: res.data.content,
            count: res.data.totalElements
        };
    }

    async addProduct(product) {
        return await axios.post(constants.url + '/api/product', {...product}, {headers: {"Authorization": "Bearer " + constants.token}});
    }

    async updateProduct(product) {
        return await axios.put(constants.url + '/api/product', {...product}, {headers: {"Authorization": "Bearer " + constants.token}});
    }

    async deleteProduct(id) {
        return await axios.delete(constants.url + '/api/product/' + id, {headers: {"Authorization": "Bearer " + constants.token}});
    }
}

export const productService = new ProductService();